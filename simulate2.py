import numpy as np
import cIntegratorSimple
import ForceFields
import os, time
import h5py
import logging

def genrandint():
    'Generates a random integer between 0 and (2^32)-1'
    x = 0
    for i in range(4):
        x = (x << 8)+ord(os.urandom(1))
    return x

def main():

    print('Setting up logging')
    logging.basicConfig(filename='sim2.log',level=logging.DEBUG)

    print('Setting consts')
    NUM_BLOCKS = int(1E6)
    STEPS_PER_BLOCK = 1
    BLOCKS_PER_DUMP = 1000
    ff = ForceFields.Dickson2dPeriodicForce()
    
    MASS = 1.0
    XI = 3.0
    BETA = 1.0 
    NDIMS = 2
    DT = 0.005
    ISPERIODIC = np.array([0,1],dtype=np.int)
    BOXSIZE = np.array([1.0E8,1.0])

    print('Instantiating Integrator')
    integrator = cIntegratorSimple.SimpleIntegrator(ff,MASS,XI,BETA,DT,NDIMS,ISPERIODIC,BOXSIZE,genrandint())

    # Setup hdf5 for storage
    print('Setting up HDF5')
    f = h5py.File('traj.h5','w')
    coords = f.create_dataset('coords',(NUM_BLOCKS,NDIMS),compression='gzip')
    
    # Allocate numpy storage for temp storage of positions
    ctemp = np.zeros((BLOCKS_PER_DUMP,2))    

    # Initial coords and velocities
    x = np.array([0.0,0.5])
    
    totblocks = NUM_BLOCKS//BLOCKS_PER_DUMP
    print('Starting Simulation')
    for dk in xrange(totblocks):
        t1 = time.time()
        for k in xrange(BLOCKS_PER_DUMP):
            integrator.step(x,STEPS_PER_BLOCK)
            ctemp[k,:] = x
            
        coords[dk*BLOCKS_PER_DUMP:(dk+1)*BLOCKS_PER_DUMP,:] = ctemp
        logging.info('Completed {} of {} steps: {} s'.format(dk,totblocks-1,time.time() - t1))




    f.close()


if __name__ == '__main__':
    main()

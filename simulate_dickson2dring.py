import numpy as np
import cIntegratorSimple
import ForceFields
import os, sys, time
import h5py
import logging

def genrandint():
    'Generates a random integer between 0 and (2^32)-1'
    x = 0
    for i in range(4):
        x = (x << 8)+ord(os.urandom(1))
    return x

def main(beta):

    print('Setting up logging')
    logging.basicConfig(filename='sim_d2dr_{}.log'.format(beta),level=logging.DEBUG)

    print('Setting consts')
    NUM_BLOCKS = int(1E3)
    STEPS_PER_BLOCK = 10
    BLOCKS_PER_DUMP = 500
    ff = ForceFields.Dickson2dRingForce()
    
    MASS = 1.0
    XI = 1.5
    BETA = beta 
    NDIMS = 2
    DT = 0.005
    ISPERIODIC = np.array([0,0],dtype=np.int)
    BOXSIZE = np.array([1.0E8,1.0E8])

    print('Instantiating Integrator')
    integrator = cIntegratorSimple.SimpleIntegrator(ff,MASS,XI,BETA,DT,NDIMS,ISPERIODIC,BOXSIZE,3227724242L)#genrandint())

    # Setup hdf5 for storage
    print('Setting up HDF5')
    f = h5py.File('traj_d2dr_{}.h5'.format(beta),'w')
    coords = f.create_dataset('coords',(NUM_BLOCKS,NDIMS),compression='gzip')
    
    # Allocate numpy storage for temp storage of positions
    ctemp = np.zeros((BLOCKS_PER_DUMP,2))    

    # Initial coords and velocities
    x = np.array([-3.0,0.0])
    
    totblocks = NUM_BLOCKS//BLOCKS_PER_DUMP
    print('Starting Simulation')
    for dk in xrange(totblocks):
        t1 = time.time()
        for k in xrange(BLOCKS_PER_DUMP):
            integrator.step(x,STEPS_PER_BLOCK)
            ctemp[k,:] = x
            
        coords[dk*BLOCKS_PER_DUMP:(dk+1)*BLOCKS_PER_DUMP,:] = ctemp
        logging.info('Completed {} of {} steps: {} s'.format(dk,totblocks-1,time.time() - t1))




    f.close()


if __name__ == '__main__':
    beta = float(sys.argv[1])
    main(beta)
